const express = require("express");
const WebSocket = require("ws");

const PORT = process.env.PORT || 3000

// Express server
const app = express();

app.get("/", (req, res) => {
  res.send("This is a normal http response");
});

const server = app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});

// WEBSOCKET CLIENT

function connectHome() {
  // let wsClient = new WebSocket("ws://127.0.0.1:8000/?name=moses");
  const wsClient = new WebSocket("ws://127.0.0.1:8000");
  wsClient.on("open", () => {
    console.log("Connection '/' opened");
    wsClient.send("Hey there from client");
  });

  wsClient.on("error", (error) => {
    console.log("Error---:", error.message);
  });

  wsClient.on("close", () => {
    console.log("Disconnected '/'");
    console.log("retrying...");
    setTimeout(() => {
      connectHome();
    }, 3000 + 1000);
  });

  wsClient.on("message", (data) => {
    console.log("Server res on '/':", data);
  });
}

function connectNewOrder() {
  // TODO: extract rider id from jwt token and send to server as a param
  let riderId = "5"

  const wsClient2 = new WebSocket(`ws://127.0.0.1:8000/newOrder?id=${riderId}`);
  wsClient2.on("open", () => {
    console.log("Connection '/newOrder' opened");
    // wsClient2.send("");
  });

  wsClient2.on("error", (error) => {
    console.log("Error---:", error.message);
  });

  wsClient2.on("close", () => {
    console.log("Disconnected '/newOrder'");
    console.log("retrying...");
    setTimeout(() => {
      connectNewOrder();
    }, 3000 + 1000);
  });

  // new order details
  wsClient2.on("message", (data) => {
    console.log();
    console.log("Server res on '/newOrder':", JSON.parse(data));
  });
}

function connectLoc() {
  const latLng = {
    lat: 56.76001,
    lng: 30.56234,
  }

  const randomizeLatlng = () => {
    return {
      lat: Math.random() * 90,
      lng: Math.random() * 180,
    }
  }

  // let wsClient = new WebSocket("ws://127.0.0.1:8000/?name=moses");
  const wsClient = new WebSocket(`ws://127.0.0.1:8000/location/update?lat=${latLng.lat}&lng=${latLng.lng}`);
  wsClient.on("open", () => {
    console.log("Connection '/location/update' opened");
    // wsClient.send("Hey there from client");
    // wsClient.send(JSON.stringify({ riderId: 62, latLng }));

    setInterval(() => {
      const newLatlng = randomizeLatlng();

      wsClient.send(JSON.stringify({ riderId: 62, latLng: newLatlng }));
    }, 2500);
  });

  wsClient.on("error", (error) => {
    console.log("Error---:", error.message);
  });

  wsClient.on("close", () => {
    console.log("Disconnected '/location/update'");
    console.log("retrying...");
    setTimeout(() => {
      connectLoc();
    }, 3000 + 1000);
  });

  wsClient.on("message", (data) => {
    // TODO: check for errors in 'data' and act accordingly eg. clear interval for updating rider location

    console.log("Server res on '/location/update':", data);
  });
}



// trigger the connections
// connectHome();
// connectNewOrder();
connectLoc();
